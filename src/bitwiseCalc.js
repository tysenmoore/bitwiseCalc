
(function() { // IIFE
'use strict';

//============================================================================
// Module Definition
//============================================================================
angular.module('bitwiseApp',
               [ 'inputDec',
                 'inputHex',
                 'ngByte',
                 'ngSanitize',
                 'ui.bootstrap' ])
//============================================================================
// bitwiseCtrl Controller Definition
//============================================================================
.controller('bitwiseCtrl',
            [ '$document',
              '$scope',
              '$uibModal',
                bitwiseCtrl ]);

//============================================================================
// bitwiseCtrl Controller
//============================================================================
function bitwiseCtrl( $document,
                      $scope,
                      $uibModal) {

    //-------------------------------------------------------------------------
    // Member Attributes
    //-------------------------------------------------------------------------
    var vm              = this;

    vm.$scope           = $scope;

    vm.aboutDlgInst     = null;
    vm.isExtByteOpen    = false;

    vm.bitsUsed         = 32; // 32 | 16 | 8
    vm.bitsUsedSz       = [ "32", "16", "8" ];
    vm.isBitsUsedOpen   = false;
    vm.hexMax           = Math.pow(2, vm.bitsUsed)-1;  // e.g. 8-bit == 255
    vm.hexPrecision     = vm.bitsUsed / 4;

    vm.isSigned         = false;

    vm.opSel            = "AND";
    vm.ops              = [ "AND", "OR", "NAND", "NOR", "XOR", "MOD" ];

    vm.shiftChoices     = [ ];
    vm.isShiftOpen      = [ false, false ];
    vm.shiftChoice      = [ 1, 1 ];

    vm.data             = [
        // Top bytes
        {
            iDec:  0,  // decimal value
            hDec:  "", // hex value
            bytes: [0,0, 0,0]
        },
        // Bottom bytes
        {
            iDec:  0,  // decimal value
            hDec:  "", // hex value
            bytes: [0,0, 0,0]
        },
        // Calculated bytes
        {
            iDec:  0,  // decimal value
            hDec:  "", // hex value
            bytes: [0,0, 0,0]
        }
    ];

    //-------------------------------------------------------------------------
    // Member Function Prototypes
    //-------------------------------------------------------------------------
    vm.about            = about;
    vm.actionBtn        = actionBtn;
    vm.applyOp          = applyOp;
    vm.bitsUsedChanged  = bitsUsedChanged;
    vm.byteChanges      = byteChanges;
    vm.decChanged       = decChanged;
    vm.hexChanged       = hexChanged;
    vm.range            = range;
    vm.signChange       = signChange;


    //=========================================================================
    // Main
    //=========================================================================

    bitsUsedChanged();

    //=========================================================================
    // Private Member Function Implementation
    //=========================================================================

    function _applySigned( lvalue ) {

        var maxMask = Math.pow(2, vm.bitsUsed)-1;
        var maxVal;
        var value   = lvalue;

        // First apply the mask
        value = (value & maxMask) >>> 0; // keep as unsigned int

        // Now apply the signed bit
        if (vm.isSigned) {

            maxVal = Math.pow(2, (vm.bitsUsed- 1)) - 1;
            if (value > maxVal) {
                value = value - (maxMask+1);
            }
        }
        else if (value < 0) {
            // Unsigned but somehow lower than zero
            value = Math.abs(value);
        }

        return value;

    } // _applySigned

    function _toHex( lVal, lpadLen, nanVal ) {

        // nanVal == what to return for a NaN value

        var len;
        var padLen = lpadLen;
        var val    = lVal;
        var sVal;

        if (angular.isUndefined( val )) {
            return "";
        }

        if (typeof val == 'string') {
            val = parseInt( val, 0 );
        }

        if (typeof padLen == 'string') {
            padLen = parseInt( padLen, 0 );
        }

        sVal = (val < 0 ? (0xFFFFFFFF + val + 1) : val).toString(16);

        if (angular.isDefined( padLen )) {

            if (sVal.length < padLen) {
                // +1 because the Array gives one less that you want
                len  = (padLen - sVal.length)+1;
                sVal = Array(len).join("0") + sVal;
            }
        }

        if ((sVal === "NaN") && (angular.isString(nanVal))) {
            return nanVal;
        }

        return sVal.toUpperCase();

    } // _toHex

    //=========================================================================
    // Member Function Implementation
    //=========================================================================

    function about() {

        vm.aboutDlgInst = $uibModal.open({
            animation:       true,
            ariaLabelledBy:  'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl:     'aboutContent.html',
            controller:      'aboutController',
            controllerAs:    'ctrl',
            //size:            size,
            //appendTo:        parentElem
        });

        vm.aboutDlgInst.result.then( function(selectedItem) {
            console.info('Modal dismissed at: ' + new Date());
        });

    } // about

    function aboutOk() {

        vm.aboutDlgInst.close();

    } // aboutOK

    function actionBtn( intRef, action ) {

        switch (action) {
        case "set":
            vm.data[intRef].hDec = 0xFFFFFFFF;
            break;
        case "clear":
            vm.data[intRef].hDec = 0;
            break;
        case "inv":
            vm.data[intRef].hDec = ~vm.data[intRef].iDec;
            break;
        case "endian":
            if (vm.bitsUsed === 32) {
                vm.data[intRef].hDec = ((vm.data[intRef].iDec & 0xFF) << 24)  |
                                       ((vm.data[intRef].iDec & 0xFF00) << 8) |
                                       ((vm.data[intRef].iDec >> 8) & 0xFF00) |
                                       ((vm.data[intRef].iDec >> 24) & 0xFF);
            }
            else if (vm.bitsUsed === 16) {
                vm.data[intRef].hDec = ((vm.data[intRef].iDec & 0xFF) << 8) |
                                       ((vm.data[intRef].iDec >> 8) & 0xFF);
            }
            // else, NOP for 8-bit
            break;
        case "shl":
            vm.data[intRef].hDec = vm.data[intRef].iDec << vm.shiftChoice[intRef];
            break;
        case "shr":
            vm.data[intRef].hDec = vm.data[intRef].iDec >> vm.shiftChoice[intRef];
            break;
        case "dec":
            vm.data[intRef].hDec--;
            break;
        case "inc":
            vm.data[intRef].hDec++;
            break;
        }

        // Apply new mask
        vm.data[intRef].hDec = vm.data[intRef].hDec & vm.hexMax;

        /* Trigger the update within the rest of the HMI */
        hexChanged( intRef );

    } // actionBtn

    function applyOp( intRef ) {

        var idx;

        if (intRef >= 2) {
            return;
        }

        switch (vm.opSel) {
        case "AND":
            vm.data[2].hDec = vm.data[0].iDec & vm.data[1].iDec;
            break;
        case "OR":
            vm.data[2].hDec = vm.data[0].iDec | vm.data[1].iDec;
            break;
        case "NAND":
            vm.data[2].hDec = !(vm.data[0].iDec & vm.data[1].iDec);
            break;
        case "NOR":
            vm.data[2].hDec = !(vm.data[0].iDec | vm.data[1].iDec);
            break;
        case "XOR":
            vm.data[2].hDec = vm.data[0].iDec ^ vm.data[1].iDec;
            break;
        case "MOD":
            vm.data[2].hDec = vm.data[0].iDec % vm.data[1].iDec;
            break;
        }

        // Update the bits for the calculated bytes
        hexChanged( 2 );

    } // applyOp

    // Call when the number of bits change
    function bitsUsedChanged() {

        var x;

        vm.hexMax       = Math.pow(2, vm.bitsUsed)-1;
        vm.hexPrecision = vm.bitsUsed / 4;

        vm.shiftChoices = [ ];
        for (x = 1; x < vm.bitsUsed; x++) {
            vm.shiftChoices.push( x );
        }

        for (x = 0; x < vm.shiftChoice.length; x++) {
            if (vm.shiftChoice[x] > (vm.bitsUsed-1)) {
                vm.shiftChoice[x] = (vm.bitsUsed - 1);
            }
        }

        // Apply new mask
        for (x = 0; x < 3; x++) {
            vm.data[x].hDec = vm.data[x].iDec & vm.hexMax;
            vm.data[x].hDec = _applySigned( vm.data[x].hDec );
            hexChanged( x );
        }

    } // bitsUsedChanged

    // Called when bits change
    function byteChanges( intRef, byteIdx, byteVal ) {

        var idx;
        var tmp;
        var val = 0;

        //console.log("BYTE change.PRE", intRef, vm.data[intRef].iDec, byteIdx, byteVal);

        vm.data[intRef].bytes[byteIdx-1] = byteVal;

        for (idx = 0; idx < 4; idx++) {
            // force this to be a signed number, bitwise
            // operations result in a signed 32bit value.
            tmp = (vm.data[intRef].bytes[idx] << (8 * idx)) >>> 0;
            val += tmp;
        }

        vm.data[intRef].iDec = _applySigned(val);

        decChanged( intRef );

        //console.log("BYTE change.POST", vm.data[intRef].iDec);

    } // byteChanges

    function decChanged( intRef ) {

        // NOTE: vm.data[intRef].iDec already changed

        //console.log("decCHanged:", intRef, vm.data[intRef].iDec, vm.data[intRef].bytes);

        var idx;

        if ((vm.data[intRef].iDec === null) ||
            (angular.isUndefined(vm.data[intRef].iDec))) {
            return;  //vm.data[intRef].iDec = 0;
        }

        for (idx = 0; idx < 4; idx++) {
            vm.data[intRef].bytes[idx] = ((vm.data[intRef].iDec >> (8*idx)) & 0xFF);
        }

        if (!vm.isSigned) {
            // This will convert to an unsigned int
            vm.data[intRef].iDec = vm.data[intRef].iDec >>> 0;
        }

        vm.data[intRef].hDec = "0x"+_toHex( vm.data[intRef].iDec, (vm.bitsUsed/8) );

        applyOp( intRef );

    } // decChanged

    function hexChanged( intRef ) {

        // NOTE: vm.data[intRef].hDec already changed

        // NOTE: vm.data[intRef].hDec comes in as a decimal value on change
        //       and a string onBlur.
        if (angular.isString(vm.data[intRef].hDec)) {
            vm.data[intRef].iDec = parseInt(vm.data[intRef].hDec, 0);
        }
        else {
            vm.data[intRef].iDec = vm.data[intRef].hDec;
        }

        vm.data[intRef].iDec = _applySigned(vm.data[intRef].iDec);
        vm.data[intRef].hDec = vm.data[intRef].iDec;

        applyOp( intRef );
        decChanged( intRef );

    } // hexChanged

    function range(nRange) {
        // Hack for ng-repeat to allow length of arrays to be used
        // e.g. <li ng-repeat="i in range(5)"></li>
        return new Array(nRange);

    } // range

    function signChange() {

        var x;

        // Apply new mask
        for (x = 0; x < 2; x++) {
            hexChanged( x );
        }

        applyOp( 0 );

    } // signChange

} // bitwiseCtrl

})(); // IIFE
