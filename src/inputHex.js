// angular/no-directive-replace must be off because if the "replace"
// is removed the filler section no longer works.
/* eslint-disable angular/file-name, angular/component-limit, angular/no-directive-replace */

//=============================================================================
//  Implements a generic way to validate an input as hex.
//
//  Example:
//
//  <input type="text"
//      name="nameText"
//      required
//      class="form-control input-sm netBldrItemName"
//      ng-model="nbstrmView.netBldrData.cache.selectedItem.nodes.vid"
//      input-hex
//      hex-precision="4"
//      hex-max="0x1C"
//      hex-change="changeCallback()
//      hex-blur="blurCallback()">
//
// ----------------------------------------------------------------
//
// hex-blur      [optional]    Function callback when the input has changes
//                             to a blur state or the Enter is pressed.
//                             This is where any mask values should be applied.
//                             (Invalid values will not call this callback)
//
// hex-change    [optional]    Function callback when the hex value changes
//                             (Invalid values will not call this callback)
//
// hex-max       [optional]    Max value allowed (can be hex or decimal)
//                             This is tracked live and can change dynamically.
//                             (other example, hex-max="{{bitObj.mask}}")
//                             (default = 255)
//
// hex-precision [optional]    Number of hex digits to pad
//                             If set to 1, [0..F] supported, no leading
//                             'x' or '0x' needed to indicate hex value
//                             This is tracked live and can change dynamically.
//                             (other example, hex-precision="{{bitObj.precision}}")
//                             (default = 2)
//
// ign-prefix    [optional]    Do not display the leading "0x" (default=false)
//
// nan-value     [optional]    What to display on a NaN value.
//
//=============================================================================

(function() { // IIFE

'use strict';

angular
    .module('inputHex', [])
    // Directives
    .directive('inputHex',
               [ '$window',
               inputHex ]);

function _toHex( lVal, lpadLen, nanVal ) {

    // nanVal == what to return for a NaN value

    var len;
    var padLen = lpadLen;
    var val    = lVal;
    var sVal;

    if (angular.isUndefined( val )) {
        return "";
    }

    if (typeof val == 'string') {
        val = parseInt( val, 0 );
    }

    if (typeof padLen == 'string') {
        padLen = parseInt( padLen, 0 );
    }

    sVal = (val < 0 ? (0xFFFFFFFF + val + 1) : val).toString(16);

    if (angular.isDefined( padLen )) {

        if (sVal.length < padLen) {
            // +1 because the Array gives one less that you want
            len  = (padLen - sVal.length)+1;
            sVal = Array(len).join("0") + sVal;
        }
    }

    if ((sVal === "NaN") && (angular.isString(nanVal))) {
        return nanVal;
    }

    return sVal.toUpperCase();

} // _toHex

function _validHex( sVal, hexPrecision, lHexPrefix, nanValue ) {

    // sVal, string value, if not a string then an error
    //
    // hexPrecision, number of digits for padding length,
    //               if undefined or zero, no padding
    //
    // lHexPrefix, What to prefix for the hex output
    //             if undefined it defaults to "0x".  To disable, use ""
    //
    // Returns:
    // { badVal:     true=value is bad, false=values are good
    //   decVal:     decimal value, NaN if bad, else valid
    //   hexVal:     hex string value, undefined if bad, else valid,
    //   isDec:      based on sVal input, decimal or hex
    //               true='value' is decimal, false='value' is hex
    //   value:      string value (hex or deciaml)
    // }

    // These are some test results:
    // (See inputText test() to execute tests)
    // { val:"1x10",    dec:NaN,   badVal: true },
    // { val:"1x",      dec:NaN,   badVal: true },
    // { val:"0xx10",   dec:NaN,   badVal: true },
    // { val:"xx10",    dec:NaN,   badVal: true },
    //
    // { val:"0xFF",    dec:255,   badVal: false },
    // { val:"xFF",     dec:255,   badVal: false },
    // { val:"FF",      dec:NaN,   badVal: true },
    // { val:"0xFG",    dec:NaN,   badVal: true },
    //
    // { val:"0x10",    dec:16,    badVal: false },
    // { val:"x10",     dec:16,    badVal: false },
    // { val:"10",      dec:10,    badVal: false },
    // { val:"",        dec:NaN,   badVal: true },
    // { val:"x",       dec:NaN,   badVal: true },
    // { val:"0x",      dec:NaN,   badVal: true },
    // { val:"0x220",   dec:544,   badVal: false },
    // { val:"x220",    dec:544,   badVal: false },
    // { val:"220",     dec:220,   badVal: false }

    var hexVal;
    var decVal = 0;
    var isDec  = false; // is sVal considered decimal
    var mObj;
    var hObj;
    var retVal = sVal;
    var hexPrefix = lHexPrefix;

    if ( !angular.isString( sVal ) ) {
        return { badVal:     true,
                 decVal:     NaN,
                 isDec:      false };
    }

    if (angular.isUndefined(hexPrefix)) {
        hexPrefix = "0x";
    }

    mObj = sVal.match( /(^0?x?)([\da-fA-F]*)/i );

    // 0xFG match ["0xF", "0x", "F"]
    // 1x10 match ["1","","1"]
    // 0x10 match ["0x10","0x","10"]
    // x10  match ["x10","x","10"]
    // 10   match ["10","","10"]
    //console.log( sVal+" match", JSON.stringify(mObj) );

    if (mObj[0] !== sVal) {
        // Error
        decVal = NaN;
    }
    else if ((mObj[1].toUpperCase() === "0X") ||
             (mObj[1].toUpperCase() === "X")  ||
             (hexPrecision === 1)             ||
             (hexPrecision === "1") ) {
        // MUST be hex value
        decVal = parseInt( mObj[2], 16 );

        if ( !isNaN(decVal) ) {
            // Deal with leading zeros
            hObj = mObj[2].match(/(0*)([\da-fA-F]*)/i);

            // ["00a","00","a"]
            // ["00","00",""]
            // ["0","0",""]
            // ["a","","a"]
            //console.log("leading 0: ", angular.toJson(hObj));

            if (( hObj[1].length !== 0 ) && ( hObj[2].length !== 0 )) {
                retVal = hexPrefix + _toHex( decVal, hexPrecision, nanValue );
            }
        }
    }
    else if (sVal === "0") {
        // if only zero mObj == ["0", "0", ""]
        // so allow a zero as a good value
        decVal = 0;
        isDec  = true;
    }
    else if ( (mObj[1] === "") || (mObj[1] === "0") ) {
        // If mObj[1] === "0", must have leading zero "01"
        // Assume it is decimal value
        decVal = parseInt( mObj[2], 10 );
        isDec  = true;

        if ((mObj[1] === "0") && (!isNaN(decVal))) {
            retVal = decVal.toString();
        }
    }
    else {
        // Error
        decVal = NaN;
    }

    if ( !isNaN(decVal) ) {
        hexVal = hexPrefix + _toHex( decVal, hexPrecision, nanValue );
        if ( !isDec ) {
            retVal = hexVal;
        }
    }

    return { badVal:     isNaN(decVal),
             decVal:     decVal,
             hexVal:     hexVal,
             isDec:      isDec,
             value:      retVal };

} // _validHex

function _staticCheck( scope, value ) {

    var hexPrecision;
    var undef;
    var retObj;
    var retVal = value;

    if (!scope.dArgs.inFocus) {
        hexPrecision = scope.dArgs.hexPrecision;
    }

    retObj = _validHex(value, hexPrecision, undef, scope.nanValue);

    if ( retObj.badVal ) {

        if (angular.isUndefined(hexPrecision)) {
            hexPrecision = 2;
        }

        retVal = "0x" + Array(hexPrecision+1).join("0");
    }
    else if ( (angular.isDefined(scope.dArgs.hexMax)) &&
              (retObj.decVal > scope.dArgs.hexMax) ) {

        // Max value exceeded, limit it
        retObj.decVal = (retObj.decVal & scope.dArgs.hexMax);
        retVal = "0x" + _toHex( retObj.decVal,
                                hexPrecision,
                                scope.nanValue );
    }
    else {
        retVal = retObj.hexVal;
    }

    return retVal;

} // _staticCheck

function inputHex( $window ) {

    //  'A' - <span ng-sparkline></span>

    return {
        restrict:   'A',
        require:    [ 'ngModel' ],
        scope:      {
            hexBlur:      '&',
            hexChange:    '&',
            hexMax:       '=',
            hexPrecision: '=',
            ignPrefix:    '@',
            nanValue:     '@'
        },
        link: function(scope, elem, attr, ngModel) {

            //-----------------------------------------------------------------
            function _dom2model( value ) {

                var hexPrecision;
                var undef;
                var retObj;
                var retVal;

                if (!scope.dArgs.inFocus) {
                    hexPrecision = scope.dArgs.hexPrecision;
                }

                retObj = _validHex( value, hexPrecision,
                                    undef, scope.dArgs.nanValue );

                if ( ( !retObj.badVal ) ||
                     (value.toUpperCase() === "0X") ||
                     (value.toUpperCase() === "X")) {

                    if ( (angular.isDefined(scope.dArgs.hexMax)) &&
                         (retObj.decVal > scope.dArgs.hexMax) ) {
                        // Don't update the lastGoodVal
                    }
                    else {
                        // Use retObj.value in case of "01",
                        // will remove the leading zero
                        scope.dArgs.lastGoodVal = retObj.value;
                    }
                }
                else if ( retObj.badVal ) {

                    if ( value === "" ) {
                        // Allow empty string so they can correct it
                        scope.dArgs.lastGoodVal = value;
                        retObj.badVal = false;
                    }
                }

                ngModel[0].$setValidity( 'inputHex', retObj.badVal );

                // Change the value back to the last good value
                if ((scope.dArgs.ignPrefix === "true") &&
                    (scope.dArgs.lastGoodVal.indexOf("0x") !== -1) &&
                    (!scope.dArgs.inFocus)) {
                    // remove the "0x"
                    ngModel[0].$setViewValue( scope.dArgs.lastGoodVal.substr(2) );
                }
                else {
                    ngModel[0].$setViewValue( scope.dArgs.lastGoodVal );
                }

                ngModel[0].$render();

                if ( (scope.dArgs.lastGoodVal !== "") &&
                     ( !retObj.badVal ) &&
                     (angular.isFunction(scope.dArgs.hexChange)) ) {

                     scope.$evalAsync( function() {
                         scope.dArgs.hexChange();
                     });
                }

                // Keep the last valid value, return an int so the
                // ngModel value stays an int and not a string.
                retVal = parseInt(scope.dArgs.lastGoodVal, 0);
                if ( (isNaN(retVal)) && (scope.dArgs.ignPrefix === "true") ) {
                    return parseInt("0x"+scope.dArgs.lastGoodVal, 0);
                }
                return retVal;

            } // _dom2model

            //-----------------------------------------------------------------
            function _onBlur() {

                var lastGoodVal;

                scope.dArgs.inFocus     = false;
                scope.dArgs.lastGoodVal = _staticCheck( scope,
                                                        ngModel[0].$viewValue );
                lastGoodVal = scope.dArgs.lastGoodVal;

                if ( (scope.dArgs.ignPrefix === "true") &&
                     (lastGoodVal.indexOf("0x") !== -1) ) {
                    // remove the "0x"
                    lastGoodVal = lastGoodVal.substr( 2 );
                }

                // Change the value back to the last good value
                ngModel[0].$setViewValue( lastGoodVal );
                ngModel[0].$render();


                if (lastGoodVal !== "") {

                    if ( angular.isFunction(scope.dArgs.hexChange) ) {
                        scope.$evalAsync(function() {
                            scope.dArgs.hexChange();
                        });
                    }
                    if ( angular.isFunction(scope.dArgs.hexBlur) ) {
                        scope.$evalAsync(function() {
                            scope.dArgs.hexBlur();
                        });
                    }
                }

            } // _onBlur
            //-----------------------------------------------------------------

            // Local values that can be modified since the scope values cannot
            scope.dArgs = { inFocus:        false,
                            hexBlur:        scope.hexBlur,
                            hexChange:      scope.hexChange,
                            hexPrecision:   2,
                            hexMax:         255,
                            ignPrefix:      false,
                            lastGoodVal:    "" };

            if ( angular.isString(scope.hexPrecision) ) {
                scope.dArgs.hexPrecision = parseInt( scope.hexPrecision, 0 );
            }
            else if ( angular.isNumber(scope.hexPrecision) ) {
                scope.dArgs.hexPrecision = scope.hexPrecision;
            }

            if ( angular.isString(scope.ignPrefix) ) {
                scope.dArgs.ignPrefix = scope.ignPrefix;
            }

            if ( angular.isString(scope.hexMax) ) {
                scope.dArgs.hexMax = parseInt(scope.hexMax, 0);
            }

            if ( angular.isString(scope.nanValue) ) {
                scope.dArgs.nanValue = scope.nanValue;
            }

            //-----------------------------------------------------------------
            elem[0].onkeypress = function( lel) {

                var el = lel;
                var keyCode;

                if ( !el ) {
                    el = $window.event;
                }
                keyCode = el.keyCode || el.which;

                if ( keyCode === 13 ) {
                    // Enter pressed
                    if ( scope.dArgs.ignPrefix !== "true" ) {
                        // Ignore if ignPrefix so the digit is
                        // not assumed to be decimal
                        _onBlur();

                        // Really still in focus, we only hit enter
                        scope.dArgs.inFocus = true;
                    }
                }
            };
            //-----------------------------------------------------------------

            //-----------------------------------------------------------------
            scope.$watch('hexMax', function(val) {

                var retVal;

                if ( angular.isUndefined(val) ) {
                    return;
                }

                // Track changes in the value
                scope.dArgs.hexMax = val;

                retVal = _dom2model( scope.dArgs.lastGoodVal );

                // If the precision changes, we need to check the value
                scope.dArgs.lastGoodVal = _staticCheck( scope, scope.dArgs.lastGoodVal );
                ngModel[0].$setViewValue( scope.dArgs.lastGoodVal );
                ngModel[0].$render();
            });
            scope.$watch('hexPrecision', function(val) {

                var retVal;

                if ( angular.isUndefined(val) ) {
                    return;
                }

                // Track changes in the value
                if ( angular.isString(val) ) {
                    scope.dArgs.hexPrecision = parseInt( val, 0 );
                }
                else if ( angular.isNumber(val) ) {
                    scope.dArgs.hexPrecision = val;
                }

                retVal = _dom2model( scope.dArgs.lastGoodVal );

                // If the precision changes, we need to check the value
                scope.dArgs.lastGoodVal = _staticCheck( scope, scope.dArgs.lastGoodVal );
                ngModel[0].$setViewValue( scope.dArgs.lastGoodVal );
                ngModel[0].$render();
            });
            //-----------------------------------------------------------------

            //-----------------------------------------------------------------
            elem.bind( 'blur', function () {
                // Do one more check on blur, and convert to hex
                // If bad, change to lastGoodValue
                _onBlur();
            });
            //-----------------------------------------------------------------

            //-----------------------------------------------------------------
            elem.bind( 'focus', function () {
                scope.dArgs.inFocus = true;

                if ((scope.dArgs.ignPrefix === "true") &&
                    (scope.dArgs.lastGoodVal.indexOf("0x") === -1) &&
                    (scope.dArgs.hexPrecision > 1)) {

                    scope.dArgs.lastGoodVal = "0x" + scope.dArgs.lastGoodVal;

                    // Change the value back to the last good value
                    ngModel[0].$setViewValue( scope.dArgs.lastGoodVal );
                    ngModel[0].$render();
                }
            });
            //-----------------------------------------------------------------

            //For DOM -> model validation
            // (convert data from view format to model format)
            //-----------------------------------------------------------------
            ngModel[0].$parsers.unshift( function(value) {

// Apply this later on a blur
//              if ((scope.dArgs.ignPrefix === "true") &&
//                  (value.indexOf("0x") === -1) &&
//                  (scope.dArgs.hexPrecision > 1)) {
//                  return _dom2model( "0x" + value );
//              }

                return _dom2model( value );
            });
            //-----------------------------------------------------------------

            // For model -> DOM validation
            // (convert data from model format to view format)
            //-----------------------------------------------------------------
            ngModel[0].$formatters.unshift( function(lvalue) {

                var value = lvalue;
                var undef;

                // value is a decimal string
                if ( scope.dArgs.hexPrecision === 1 ) {
                    // Initial value always assumed decimal, not hex
                    value = "0x" + _toHex(value, undef, scope.dArgs.nanValue);
                }

                scope.dArgs.lastGoodVal = _staticCheck( scope, value );

                ngModel[0].$setValidity( 'inputHex', true );

                if ((scope.dArgs.ignPrefix === "true") &&
                    (scope.dArgs.lastGoodVal.indexOf("0x") !== -1) &&
                    ((!scope.dArgs.inFocus) || (scope.dArgs.hexPrecision === 1))
                   ) {
                    // remove the "0x"
                    scope.dArgs.lastGoodVal = scope.dArgs.lastGoodVal.substr(2);
                    return scope.dArgs.lastGoodVal;
                }

                return scope.dArgs.lastGoodVal;
            });
        }
    };

} // inputHex


function test( ) {  // eslint-disable-line no-unused-vars

    var lst = [ { val:"1x10",    dec:NaN,   badVal: true },
                { val:"1x",      dec:NaN,   badVal: true },
                { val:"0xx10",   dec:NaN,   badVal: true },
                { val:"xx10",    dec:NaN,   badVal: true },

                { val:"0xFF",    dec:255,   badVal: false },
                { val:"xFF",     dec:255,   badVal: false },
                { val:"FF",      dec:NaN,   badVal: true },
                { val:"0xFG",    dec:NaN,   badVal: true },

                { val:"0x10",    dec:16,    badVal: false },
                { val:"x10",     dec:16,    badVal: false },
                { val:"10",      dec:10,    badVal: false },
                { val:"",        dec:NaN,   badVal: true },
                { val:"x",       dec:NaN,   badVal: true },
                { val:"0x",      dec:NaN,   badVal: true },
                { val:"0x220",   dec:544,   badVal: false },
                { val:"x220",    dec:544,   badVal: false },
                { val:"220",     dec:220,   badVal: false } ];
    var out;
    var retObj;
    var tstObj;
    var valMatch;
    var x;

    for (x = 0; x < lst.length; x++) {

        tstObj = lst[x];
        retObj = _validHex( tstObj.val );

        valMatch = false;
        if (retObj.badVal) {
            // Check for NaN
            if (isNaN(tstObj.dec) && isNaN(retObj.decVal)) {
                valMatch = true;
            }
        }
        else if (tstObj.dec === retObj.decVal) {
            valMatch = true;
        }

        out = "conv: " + tstObj.val + " to: 0x" + _toHex(retObj.decVal, 2) + " (" + retObj.decVal + ") badVal: " + retObj.badVal;
        if ((tstObj.badVal === retObj.badVal) && (valMatch)) {
            console.log("PASS: "+out); // eslint-disable-line angular/log
        }
        else {
            // Unexpected results
            console.error("ERROR: "+out); // eslint-disable-line angular/log
        }

    } // test loop

} // test


})(); // IIFE
