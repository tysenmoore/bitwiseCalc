// angular/no-directive-replace must be off because if the "replace"
// is removed the filler section no longer works.
/* eslint-disable angular/file-name, angular/component-limit, angular/no-directive-replace */

//=============================================================================
//  Implements a generic way to validate an input as decimal.  This element
//  will handle 8/16/32 bit numbers as well as signed/unsigned--which is not
//  natively handled by the default input element.
//
//  Example:
//
//  <input type="text"
//      class="form-control input-sm "
//      ng-model="someVar"
//      input-dec
//      bits="8"
//      signed="signedVar"
//      dec-change="callback()">
//
// ----------------------------------------------------------------
//
// dec-change    [optional]    Function callback when the decimal value changes
//                             (Invalid values will not call this callback)
//
// bits          [optional]    Number of bits use 8/16/32.  This value is
//                             watched
//                             (default = 32)
//
// signed        [optional]    Where the value is signed or unsigned. This value
//                             is watched.
//                             (default=false)
//
//=============================================================================

(function() { // IIFE

'use strict';

angular
    .module('inputDec', [])
    // Directives
    .directive('inputDec',
               [ '$window',
               inputDec ]);

function inputDec( $window ) {

    //  'A' - <span ng-sparkline></span>

    return {
        restrict:   'A',
        require:    [ 'ngModel' ],
        scope:      {
            decChange:    '&',
            bits:         '=',
            signed:       '='
        },
        link: function(scope, elem, attr, ngModel) {

            // Local values that can be modified since the scope values cannot
            scope.dArgs = { decChange:      scope.decChange,
                            bits:           scope.bits || 32,
                            signed:         scope.signed || false };

            if ( angular.isString(scope.bits) ) {
                scope.dArgs.bits = parseInt( scope.bits, 0 );
            }
            else if ( angular.isNumber(scope.bits) ) {
                scope.dArgs.bits = scope.bits;
            }

            // local vars
            scope.ctrl = {
                maxVal:   Math.pow(2, scope.dArgs.bits),
                minVal:   0
            }

            function _applySigned( lvalue ) {

                var maxMask = Math.pow(2, scope.dArgs.bits)-1;
                var value   = lvalue;

                // First apply the mask
                value = (value & maxMask) >>> 0; // keep as unsigned int

                // Now apply the signed bit
                if (scope.dArgs.signed) {

                    if (value > scope.ctrl.maxVal) {
                        value = value - (maxMask+1);
                    }
                }
                else if (value < 0) {
                    // Unsigned but somehow lower than zero
                    value = Math.abs(value);
                }

                return value;

            } // _applySigned

            //-----------------------------------------------------------------
            //For DOM -> model validation
            // (convert data from view format to model format)
            function _dom2model( lvalue ) {

                var value = lvalue;

                //console.log("_dom2model", value, ngModel[0].$viewValue, );

                if (angular.isString(value)) {
                    if ((value === "-") || (value === "")) {
                        if (!scope.dArgs.signed) {
                            ngModel[0].$setViewValue( "" );
                            ngModel[0].$render();
                            return "";
                        }
                        return value;
                    }

                    value = parseInt(value, 0);
                }

                if ((value < scope.ctrl.minVal) || (value > scope.ctrl.maxVal)) {
                    ngModel[0].$setValidity( 'inputDec', false );
                    // Return the previous value

                    ngModel[0].$setViewValue( ngModel[0].$modelValue );
                    ngModel[0].$render();
                    return ngModel[0].$modelValue;
                }

                if (angular.isFunction(scope.dArgs.decChange)) {

                     scope.$evalAsync( function() {
                         scope.dArgs.decChange();
                     });
                }

                return value;

            } // _dom2model

            function _init() {

                if (scope.dArgs.signed) {
                    scope.ctrl.maxVal = Math.pow(2, (scope.dArgs.bits-1));
                    scope.ctrl.maxVal--;
                    scope.ctrl.minVal = -scope.ctrl.maxVal;
                }
                else {
                    scope.ctrl.maxVal = Math.pow(2, scope.dArgs.bits);
                    scope.ctrl.maxVal--;
                    scope.ctrl.minVal = 0;
                }

            } // _init

            //-----------------------------------------------------------------
            function _onBlur() {

                //console.log("_onBlur", ngModel[0].$viewValue);

                if ((ngModel[0].$viewValue === null) ||
                    (angular.isUndefined(ngModel[0].$viewValue)) ||
                    (ngModel[0].$viewValue === "")) {

                    // Must have been empty, so make it zero
                    ngModel[0].$setViewValue( 0 );
                    ngModel[0].$render();
                    return;
                }

            } // _onBlur
            //-----------------------------------------------------------------

            //-----------------------------------------------------------------
            elem[0].onkeypress = function( lel) {

                var el = lel;
                var keyCode;

                if ( !el ) {
                    el = $window.event;
                }
                keyCode = el.keyCode || el.which;

                if ( keyCode === 13 ) {
                    // Enter pressed
                    if ( scope.dArgs.ignPrefix !== "true" ) {
                        // Ignore if ignPrefix so the digit is
                        // not assumed to be decimal
                        _onBlur();
                    }
                }
            };
            //-----------------------------------------------------------------

            //-----------------------------------------------------------------
            scope.$watch('bits', function(val) {

                //console.log("bits:", val);

                // Track changes in the value
                if ( angular.isString(val) ) {
                    scope.dArgs.bits = parseInt( val, 0 );
                }
                else if ( angular.isNumber(val) ) {
                    scope.dArgs.bits = val;
                }
                else {
                    scope.dArgs.bits = 32;
                }

                _init(); // reinit ranges

                ngModel[0].$setViewValue( _applySigned( ngModel[0].$viewValue ) );
                ngModel[0].$render();

                _dom2model( ngModel[0].$viewValue );

            });
            scope.$watch('signed', function(val) {

                //console.log("signed:", val);

                // Track changes in the value
                if ( angular.isUndefined(val) ) {
                    scope.dArgs.signed = true;
                }
                else {
                    scope.dArgs.signed = val;
                }

                _init(); // reinit ranges

                _dom2model( ngModel[0].$viewValue );
            });
            //-----------------------------------------------------------------

            //-----------------------------------------------------------------
            elem.bind( 'blur', function () {
                // Do one more check on blur, and convert to hex
                // If bad, change to lastGoodValue
                _onBlur();
            });
            //-----------------------------------------------------------------

            //For DOM -> model validation
            // (convert data from view format to model format)
            //-----------------------------------------------------------------
            ngModel[0].$parsers.unshift(function(value) {

                //console.log("dom2model", value, typeof value);

                return _dom2model( value );
            });
            //-----------------------------------------------------------------

            // For model -> DOM validation
            // (convert data from model format to view format)
            // Only done when the dome is created.
            //-----------------------------------------------------------------
            ngModel[0].$formatters.unshift(function(lvalue) {

                var value   = lvalue;

                // Sadly this routine is called prior to the 'bits' watch
                // when the bits change.  This routine  will have the old
                // 'bits' value.

                //console.log("model2dom", value, typeof value);

                if (angular.isString(value)) {
                    if ((value === "-") || (value === "")) {
                        return value;
                    }
                    value = parseInt(value, 0);
                }

                // When changing 'bits' the lvalue will already be
                // converted to a an unsigned version of the
                // new bit size.  If signed we need to convert it.

                return _applySigned( value );
            });

        }
    };

} // inputDec


})(); // IIFE
