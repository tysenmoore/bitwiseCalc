
(function() { // IIFE

'use strict';

//============================================================================
// Directive Definition
//============================================================================
angular
    .module('ngByte', [])
    // Directives
    .directive( 'ngByte', ngByte );

//============================================================================
// Directive
//============================================================================
function ngByte() {

    return {
        restrict:   'E',
        scope:      {
            bitsUsed:       '=',
            byteIdx:        '@',
            intRef:         '@',
            onChange:       '&',
            val:            '=',
            disabled:       '@',
        },
        link: function( scope, elem ) {

            scope.bits = [ 0,0,0,0, 0,0,0,0 ];

            if (angular.isString(scope.disabled)) {
                scope.isDisabled = true;
            }
            else {
                scope.isDisabled = false;
            }

            scope.bitChanged = function( bitIdx ) {

                //console.log("bit: ", bitIdx, scope.bits);

                var bitIdx;
                var fn;
                var val = 0;

                for (bitIdx = 0; bitIdx < 8; bitIdx++) {
                    val |= (scope.bits[bitIdx] << bitIdx);
                }
                scope.val = val;
                //console.log("val: ", scope.val);

                if ( angular.isFunction(scope.onChange) ) {
                    fn = scope.onChange();

                    if ( angular.isFunction(fn) ) {
                        fn( scope.intRef, scope.byteIdx, scope.val );
                    }
                }
            } // bitChanged

            scope.range = function(nRange) {
                // Hack for ng-repeat to allow length of arrays to be used
                // e.g. <li ng-repeat="i in range(5)"></li>
                return new Array(nRange);
            } // range

            scope.setBits = function( val ) {

                var bitIdx;
                for (bitIdx = 0; bitIdx < 8; bitIdx++) {
                    scope.bits[bitIdx] = (val >> bitIdx) & 0x01;
                }
            } // setBits

            scope.$watch('val', function(val, old) {
                scope.setBits( val );
            });

            if (angular.isUndefined(scope.val)) {
                scope.val = 0;
            }
            scope.setBits( scope.val );
        },
        template:
        '<div class="byteWell ngByteByte" ng-hide="bitsUsed < (byteIdx*8)">'+

        '  <div class="nibbleWell ngByteNibbleLo">'+
        '    <div class="ngByteBit" ng-repeat="i in range(4) track by $index">'+
        '      <input type="checkbox"'+
        '        ng-disabled="isDisabled"'+
        '        class="ngByteBitCheck"'+
        '        ng-model="bits[3-$index]"'+
        '        ng-true-value="1",'+
        '        ng-false-value="0",'+
        '        ng-click="bitChanged(3-$index)">'+
        '      <label class="ngByteBitLbl">'+
        '        {{(byteIdx*8)-(5+$index)}}'+
        '      </label>'+
        '    </div>'+
        '  </div>'+

        '  <div class="nibbleWell ngByteNibbleHi">'+
        '    <div class="ngByteBit" ng-repeat="i in range(4) track by $index">'+
        '      <input type="checkbox"'+
        '        ng-disabled="isDisabled"'+
        '        class="ngByteBitCheck"'+
        '        ng-model="bits[7-$index]"'+
        '        ng-true-value="1",'+
        '        ng-false-value="0",'+
        '        ng-click="bitChanged(7-$index)">'+
        '      <label class="ngByteBitLbl">'+
        '        {{(byteIdx*8)-(1+$index)}}'+
        '      </label>'+
        '    </div>'+
        '  </div>'+

        '</div>'

    };

} // ngByte

})(); // IIFE
