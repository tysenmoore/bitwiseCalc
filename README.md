﻿
# Bitwise Calculator
This is a bitwise calculator that makes it easy to view bytes in: hex, decimal or binary formats.  The calculator also allows for manipulations (XOR, AND, etc) of two numbers.

The calculator is written in AngularJS, UI-Bootstrap.  The project distributes a single HTML file for easy use.  This HTML file contains all dependencies, CSS, etc.  This means you can copy a single file to a location/desktop and load the tool in any browser.


## Usage

Copy your preferred themed version of the tool from `dist/bitwiseCalc_[themeName].html` to anywhere locally.  Then load the file in your browser.

Prebuilt versions are available in the `dist` directory.

**NOTE:**  Files ending in `min.html` have been minified (or made compact).


## Development

Load `src/bitwiseCalc.html` into your browser for development, then modify the necessary files.


## Release

### Javascript

**Prerequisites:** 
```
npm install
```

To generate the distribution files simply run:
```
npm run build
```

This will generate a single minified and non-minified version of the tool for **each** theme found in `src/theme`.

The release process will use the `src/bitwiseCalc.html` as its template.  The process will replace necessary files and put them inline inside the template file.  This new file will be copied to `dist` and then another copy will be minified.

### TODO

* None
