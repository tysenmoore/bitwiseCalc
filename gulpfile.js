
// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var concat      = require('gulp-concat');
var htmlmin     = require('gulp-htmlmin');
var htmlreplace = require('gulp-html-replace');
var inject_str  = require('gulp-inject-string');
var inline_src  = require('gulp-inline-source');
var rename      = require('gulp-rename');
var strip       = require('gulp-strip-comments');
var strip_css   = require('gulp-strip-css-comments');
var uglify      = require('gulp-uglify');

// Concatenate & Minify JS
gulp.task('concantMinJS', function() {
    return gulp.src(['src/bitwiseCalc.js', 'src/*.js'])
        .pipe(concat('all.js'))
        .pipe(strip())
        .pipe(gulp.dest('build'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('build'));

}); // concantMinJS

// Strip CSS
gulp.task('stripCSS', function() {
    return gulp.src('src/theme/*.css')
        .pipe(strip_css())
        .pipe(gulp.dest('build'));

}); // stripCSS

// Inject the minified sources into the html
gulp.task('injectMin', function() {

    var opts = {
        compress: false
    };

    gulp.src('src/bitwiseCalc.html')
    .pipe(htmlreplace({
        'css': '../src/theme/default.css',
        'js':  '../build/all.min.js'
    }))

    // MUST add "inline" attribute to script line for 'inline_src'
    .pipe(inject_str.after( "build/all.min.js\"",  " inline"  ))
    .pipe(inject_str.after( "theme/default.css\"", " inline"  ))
    .pipe(inline_src( opts ))

    .pipe(rename('bitwiseCalc_default.min.html'))
    .pipe(htmlmin({collapseWhitespace: true, minifyCSS: true}))
    .pipe(gulp.dest('dist/'));

}); // injectMin

// Inject the actual sources into the html
// Debuggable version (not minified)
gulp.task('injectDbg', function() {

    var opts = {
        compress: false
    };

    gulp.src('src/bitwiseCalc.html')
    .pipe(htmlreplace({
        'css': '../src/theme/default.css',
        'js':  '../build/all.js'
    }))

    // MUST add "inline" attribute to script line for 'inline_src'
    .pipe(inject_str.after( "build/all.js\"",      " inline"  ))
    .pipe(inject_str.after( "theme/default.css\"", " inline"  ))
    .pipe(inline_src( opts ))

    .pipe(rename('bitwiseCalc_default.html'))
    .pipe(gulp.dest('dist/'));

}); // injectDbg

// Default Task
gulp.task('default', ['stripCSS', 'concantMinJS', 'injectMin', 'injectDbg']);
